<?php
/**
 * Implementation of hook_helpinjector_help().
 */
function debut_image_helpinjector_help($path) {
  switch ($path) {
    case 'node/add/image':
      return theme('advanced_help_topic', 'debut_image', 'Debut-Image');
  }
}


function debut_image_helpinjector_keys() {
  return array(
    'image_node_form:title' => array('module' => 'debut_image', 'file' => 'Posting-a-new-image'),
  );

}

