
; Drupal version
core = 7.x
api = 2

; Contrib modules
projects[cck][subdir] = contrib
projects[context][subdir] = contrib
projects[ctools][subdir] = contrib
projects[debut][subdir] = contrib
projects[debut_image][subdir] = contrib
projects[features][subdir] = contrib
projects[filefield][subdir] = contrib
projects[helpinjector][subdir] = contrib
projects[imageapi][subdir] = contrib
projects[imagecache][subdir] = contrib
projects[imagefield][subdir] = contrib
projects[pathauto][subdir] = contrib
projects[strongarm][subdir] = contrib
projects[token][subdir] = contrib
projects[views][subdir] = contrib
